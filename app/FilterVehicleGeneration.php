<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\FilterVehicleGeneration
 *
 * @property int $id
 * @property int $filter_id
 * @property int $filter_vehicle_models_id
 * @property int $generation_id
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleGeneration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleGeneration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleGeneration query()
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleGeneration whereFilterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleGeneration whereFilterVehicleModelsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleGeneration whereGenerationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleGeneration whereId($value)
 * @mixin \Eloquent
 */
class FilterVehicleGeneration extends Model
{
    public $timestamps = false;
}
