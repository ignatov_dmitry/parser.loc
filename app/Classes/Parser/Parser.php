<?php


namespace App\Classes\Parser;


use App\Classes\AbstractItem\AbstractParser;

final class Parser {

    /**
     * @var AbstractParser[]
     */
    private $platforms = array();

    /**
     * @param AbstractParser $platform
     */
    public function addPlatform(AbstractParser $platform)
    {
        $this->platforms[] = $platform;
    }

    public function parseNewVehicles()
    {
        foreach ($this->platforms as $platform) {
            $platform->checkNewVehicles();
        }
    }

    public function parseAllVehicles()
    {
        foreach ($this->platforms as $platform) {
            $platform->checkAllVehicles();
        }
    }
}
