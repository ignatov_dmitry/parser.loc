<?php


namespace App\Classes\AbstractItem;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

abstract class AbstractParser {

    protected $links = array(
        'pages' => array(),
        'category_pages'  => array()
    );

    protected function doRequest(string $url, array $requestParameters = [], string $method = 'GET', array $clientParameters = []){
        $client = new Client($clientParameters);
        try {
            return $client->request($method, $url, $requestParameters)->getBody()->getContents();
        } catch (GuzzleException $e) {
            return $e;
        }
    }

    public abstract function checkNewVehicles(): void;

    public abstract function checkAllVehicles(): void;
}
