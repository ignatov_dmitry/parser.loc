<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PriceHistory
 *
 * @property int $id
 * @property int $vehicle_id
 * @property float $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|PriceHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceHistory wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceHistory whereVehicleId($value)
 * @mixin \Eloquent
 */
class PriceHistory extends Model
{
    //
}
