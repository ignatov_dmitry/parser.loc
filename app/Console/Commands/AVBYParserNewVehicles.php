<?php
namespace App\Console\Commands;

use App\Classes\Parser\AvBy;
use App\Classes\Parser\Parser;
use Illuminate\Console\Command;

class AVBYParserNewVehicles extends Command {

    /**
     * The name and signature of the console command
     *
     * @var string
     */
    protected $signature = 'avby:parse_new';


    public function handle(){
        $parser = new Parser();
        $parser->addPlatform(new AvBy());
        $parser->parseNewVehicles();
    }

}
