<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\FilterVehicleModels
 *
 * @property int $filter_id
 * @property int $category_id
 * @property int|null $year
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleModels newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleModels newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleModels query()
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleModels whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleModels whereFilterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleModels whereYear($value)
 * @mixin \Eloquent
 * @property int $id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\FilterVehicleGeneration[] $generations
 * @property-read int|null $generations_count
 * @method static \Illuminate\Database\Eloquent\Builder|FilterVehicleModels whereId($value)
 */
class FilterVehicleModels extends Model
{
    public $timestamps = false;

    public function generations()
    {
        return $this->hasMany('App\FilterVehicleGeneration');
    }
}
