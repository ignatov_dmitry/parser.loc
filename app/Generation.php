<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Generation
 *
 * @property int $id
 * @property int $mapping_id
 * @property string $name
 * @property int|null $year_from
 * @property int|null $year_to
 * @property int $category_id
 * @method static \Illuminate\Database\Eloquent\Builder|Generation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Generation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Generation query()
 * @method static \Illuminate\Database\Eloquent\Builder|Generation whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Generation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Generation whereMappingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Generation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Generation whereYearFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Generation whereYearTo($value)
 * @mixin \Eloquent
 */
class Generation extends Model
{
    public $timestamps = false;
}
