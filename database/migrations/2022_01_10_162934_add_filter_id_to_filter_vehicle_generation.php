<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFilterIdToFilterVehicleGeneration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('filter_vehicle_generations', function (Blueprint $table) {
            $table->unsignedBigInteger('filter_id')->after('id');
            $table->foreign('filter_id')
                  ->references('id')
                  ->on('filters')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('filter_vehicle_generation', function (Blueprint $table) {
            //
        });
    }
}
