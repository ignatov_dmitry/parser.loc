<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilterVehicleGenerationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filter_vehicle_generations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('filter_vehicle_models_id');
            $table->foreign('filter_vehicle_models_id')
                  ->references('id')
                  ->on('filter_vehicle_models')
                  ->onDelete('cascade');
            $table->unsignedInteger('generation_id');
        });

        Schema::table('filter_vehicle_models', function (Blueprint $table) {
            $table->dropColumn('generation_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filter_vehicle_generations');
    }
}
